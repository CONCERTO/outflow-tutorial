from outflow.core.command import Command, RootCommand
from outflow.core.logging import logger

from .tasks import GenData, PrintData, Compute


@RootCommand.subcommand()
class ComputeData(Command):
    """
    Command to compute and print some data
    """

    def add_arguments(self):
        self.add_argument(
            '--multiplier',
            help="""
            Value by which the generated value will be multiplied
            """,
            type=int,
            required=True
        )

    def setup_tasks(self):
        # instantiate tasks
        gen_data = GenData()
        print_data = PrintData()
        compute = Compute()

        # setup the workflow
        gen_data >> compute >> print_data

        # return the terminating task(s)
        return print_data



@RootCommand.subcommand()
def DataReduction():
    """
    DataReduction subcommand
    """

    logger.info("Hello from data_reduction")
