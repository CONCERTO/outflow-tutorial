DataReduction
=============================================

One Paragraph of plugin description goes here


Installation
------------

To install DataReduction:

1.  Install with pip: ``pip install tuto.data_reduction``.
2.  In ``settings.py``, add ``'tuto.data_reduction'`` to ``PLUGINS``.
3.  Run ``manage.py piper descriptor``.

Authors
-------

- Toto

License
-------

This project is licensed under the Toto License

Acknowledgments
---------------

-  Hat tip to anyone who’s code was used
-  Inspiration
-  etc