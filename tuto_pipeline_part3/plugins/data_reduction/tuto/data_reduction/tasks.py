import time

from outflow.core.logging import logger
from outflow.core.tasks.task import Task
from sqlalchemy.orm.exc import NoResultFound

from .models.computation_result import ComputationResult


@Task.as_task(with_self=True)
def Compute(self: Task, some_data: int) -> {"computation_result": int}:
    # get the session of the default database
    session = self.context.default_db.session

    multiplier = self.context.args.multiplier

    # Check if the result of the computation is already in the database
    try:
        # query the database with our model ComputationResult
        computation_result_obj = session.query(ComputationResult) \
            .filter_by(input_value=some_data, multiplier=multiplier).one()

        logger.info("Result found in database")

        # get the result from the model object (ie from the row in the table)
        computation_result = computation_result_obj.result

    except NoResultFound:
        # Result not in the database: compute the value like before
        logger.info("Result not found in database, computing result and inserting")

        # simulate a big computation
        time.sleep(3)
        computation_result = some_data * multiplier

        # create an object ComputationResult
        computation_result_obj = ComputationResult(
            input_value=some_data,
            multiplier=multiplier,
            result=computation_result
        )

        # and insert it in the database
        session.add(computation_result_obj)
        session.commit()

    # return the result for the next task
    return {"computation_result": computation_result}


@Task.as_task
def GenData() -> {"some_data": int}:
    some_data = 42
    return {"some_data": some_data}


@Task.as_task
def PrintData(computation_result: int):
    logger.info(f"Result of the computation: {computation_result}")
