# -*- coding: utf-8 -*-
from sqlalchemy import Column, INTEGER, UniqueConstraint
from outflow.core.db import DefaultBase as Base


class ComputationResult(Base):
    id_computation_result = Column(INTEGER(), primary_key=True)
    input_value = Column(INTEGER(), nullable=False)
    multiplier = Column(INTEGER(), nullable=False)
    result = Column(INTEGER(), nullable=False)

    __tablename__ = "computation_result"
    __table_args__ = (UniqueConstraint("input_value", "multiplier"),)
