import json
import os

from setuptools import find_packages
from setuptools import setup

ROOT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

REQ_FILE = os.path.join(ROOT_DIRECTORY, "requirements.txt")


def get_reqs(req_file):
    """Get module dependencies from requirements.txt."""
    if not os.path.isfile(req_file):
        raise BaseException("No requirements.txt file found, aborting!")
    else:
        with open(req_file, 'r') as fr:
            requirements = fr.read().splitlines()

    return requirements

setup(
    name='tuto.data_reduction',
    author="poppy create plugin",
    packages=find_packages(),
    install_requires=get_reqs(REQ_FILE),
    include_package_data=True,
    zip_safe=False,
    package_data={
        "": ["*.json", ]
    }
,
)